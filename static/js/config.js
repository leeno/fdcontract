/**
 * 配置一些全局的公共变量,便于生产环境和线上环境切换
 */
// eslint-disable-next-line no-unused-vars
var config = {}

var deployType = function () {
  var deployType = ''
  if (window.location.host === 'admin.metaso.cn') {
    deployType = 'PRODUCT'
  } else if (window.location.host === 'admin.metasotalaw.cn') {
    deployType = 'RELEASE'
  } else if (window.location.host === 'admin.metaso.local') {
    deployType = 'LOCAL'
  } else {
    deployType = 'TEST'
  }
  return deployType
}

if (deployType() === 'RELEASE') {
  // 测试环境配置
  config = {
    href: 'https://metasotalaw.cn',
    translateRUrl: 'https://fanyi.metasotalaw.cn',
    appid: 'wx7bae32381026968e',
    redirect_uri: 'https%3a%2f%2fmetasotalaw.cn%2flogin',
    dsn: 'https://205c47a2461048988270481d0b83c482@sentry.metasotalaw.com/12'
  }
} else if (deployType() === 'PRODUCT') {
  // 生产环境配置
  config = {
    href: 'https://metaso.cn',
    translateRUrl: 'https://fanyi.metaso.cn',
    appid: 'wx58123abae5d5658b',
    redirect_uri: 'https%3a%2f%2fmetaso.cn%2flogin',
    dsn: 'https://61de08d44b32427582cb4f68c5b215cd@sentry.metasotalaw.com/13'
  }
} else if (deployType() === 'LOCAL') {
  // 内部环境配置
  config = {
    href: 'https://metaso.local',
    translateRUrl: 'https://fanyi.metaso.cn',
    appid: 'wx58123abae5d5658b',
    redirect_uri: 'http%3a%2f%2fmetaso.local%2flogin'
  }
} else {
  config = {
    href: 'http://192.168.1.210:8085',
    translateRUrl: 'http://fanyi.192.168.1.210:8085',
    appid: 'wx7bae32381026968e',
    redirect_uri: 'https%3a%2f%2fmetasotalaw.cn%2flogin'
  }
}
