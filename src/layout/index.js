import Bread from './Bread.vue'
import HeadNav from './HeadNav.vue'
import LeftMenu from './LeftMenu.vue'
import Home from './Home.vue'
import Content from './Content.vue'

export {
  Bread,
  HeadNav,
  LeftMenu,
  Home,
  Content
}
