import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/page/Login'
import Callback from '@/components/Callback'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: '登录',
      hidden: true,
      component: Login
    },
    {
      path: '/callback',
      name: '回调',
      hidden: true,
      component: Callback
    }
  ]
})
