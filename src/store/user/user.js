
import * as mUtils from '@/utils/mUtils'
import * as types from '../mutation-types'

const state = {
  userinfo: mUtils.getStore('userinfo') || {},
  resError: '',
  saveStatus: true
}

const getters = {

}

const mutations = {
  [types.USER_SYSTEM_ABNORMAL] (state, response) {
    // eslint-disable-next-line no-extra-boolean-cast
    if (!!response) {
      state.resError = response
    }
  },
  [types.SET_SAVE_STATUS] (state, status) {
    state.saveStatus = status
  }
}

const actions = {
  [types.USER_SYSTEM_ABNORMAL] ({ commit, state }, response) {
    commit(types.USER_SYSTEM_ABNORMAL, response)
  },
  [types.SET_SAVE_STATUS] ({ commit, state }, status) {
    commit(types.SET_SAVE_STATUS, status)
  },
}

export default {
  state,
  getters,
  mutations,
  actions
}