/**
 * 关于一些过滤组件的编写
 */
import moment from 'moment'

// 时间格式化组件
const timeFormat = (Vue) => {
	Vue.filter('timeformat', (value, fmtstring) => {
		if (isNaN(value)) {
			return moment(value).format(fmtstring)
		} else if (!value) {
      return value
    } else if ((value+'').length > 10) {
      return moment(value).format(fmtstring)
    } else {
      return moment(value * 1000).format(fmtstring)
		}
	})
}

// 文件来源格式化组件
const setBracket = (Vue) => {
	Vue.filter('setbracket', (value) => {
		if (value) {
			if ( value.indexOf('(') !== -1 ) {
				return value.substr(0, value.indexOf('('))
			} else if (value.indexOf('（') !== -1) {
				return value.substr(0, value.indexOf('（'))
			} else {
				return value
			}
		}
	})
}

//自动获取焦点
//const onFocus = (Vue) => {
//	Vue.directive('focus', function (el, option) {
//		var defClass = 'el-input', defTag = 'input'
//		var value = option.value || true
//		if (typeof value === 'boolean') {
//			value = { cls: defClass, tag: defTag, foc: value }
//		} else {
//			value = { cls: value.cls || defClass, tag: value.tag || defTag, foc: value.foc || false }
//		}
//		if (el.classList.contains(value.cls) && value.foc) {
//  		el.getElementsByTagName(value.tag)[0].focus()
//  }
//	})
//}

export default (Vue) => {
  timeFormat(Vue)
  setBracket(Vue)
//	onFocus(Vue)
}
