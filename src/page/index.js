
import Login from './Login.vue'
import Admin from './Admin.vue'
import Index from './Index.vue'
import Producted from './Producted.vue'
import Projects from './Projects.vue'
import AdminProjects from './AdminProjects.vue'

export {
  Login,
  Admin,
  Index,
  Producted,
  Projects,
  AdminProjects,
}
