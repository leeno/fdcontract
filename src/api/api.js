import axios from 'axios'
import globalRouter from '@/main'
import * as types from '@/store/mutation-types'

if (location.host === '192.168.1.113:8097') {
  axios.defaults.baseURL = '/api'
} else {
  axios.defaults.baseURL = '/'
}
axios.defaults.timeout = 50000
axios.defaults.withCredentials = true

//全局拦截请求，如果返回未登录状态，直接回退到登录界面，提示用户重新登录
axios.interceptors.response.use(
  function (response) {
    if (response.data.code === 3) {
      globalRouter.$store.dispatch(types.USER_SYSTEM_ABNORMAL, response.data)
    } else if (response.data.code === 1) {
      globalRouter.$store.dispatch(types.USER_SYSTEM_ABNORMAL, response.data)
      globalRouter.$router.push({ path: '/' })
      localStorage.removeItem('userinfo')
      localStorage.removeItem('menuData')
    } else {
      if (localStorage.getItem('status')) {
        localStorage.removeItem('status')
      }
    }
    return response
  },
  function (error) {
    let errorMsg = ''
    if (error.response) {
      let status = error.response.data.code
      if (status === 401) {
        globalRouter.$router.push({ path: '/' })
        globalRouter.$message({
          type: 'warning',
          message: '您的登录信息失效，请重新登录'
        })
      }
      if (error.response.status === 500) {
        errorMsg = '服务器不可用'
      }
    }
    return Promise.reject(errorMsg)
  }
)

// 拦截所有请求发送之前，检测cookie信息
axios.interceptors.request.use(
  params => {
    if ($cookies.get('sid') && $cookies.get('sid') !== 'null' && $cookies.get('uid') && $cookies.get('uid') !== 'null') {
      if (params.params) {
        if (!params.params.sid) {
          params.params.sid = $cookies.get('sid')
        }
      } else {
        params.params = { sid: $cookies.get('sid') }
      }
      params.params.timestamp = new Date().getTime()
    }
    return params
  }
)

/**
 * 用户登录
 * @param {email,nickname}
 */
export const requestRegister = (params) => {
  return axios.post('/register', params)
}

/**
 * 获取当前登录用户信息
 * @params {uid}
 */
export const getUserInfo = (params) => {
  return axios.get(`/users/${params.uid}`)
}

/**
 * 退出账号
 */
export const requestLogout = () => {
  return axios.get('/logout')
}

/**
 * 邮箱方式登录
 * @param {login_by=account, email, pwd}
 */
export const loginByAccount = (params) => {
  return axios.post('/login', params)
}

/**
 * 注册账户
 * @param
 */
export const registerAccount = (params) => {
  return axios.post('/register', params)
}

/**
 * 通过邮箱重置密码
 * @params {email}
 */
export const resetPassword = (params) => {
  return axios.post('/user/password/reset', params)
}

/**
 * 获取问题列表
 * @params {page, page_size}
 */
export const getQuestions = (params) => {
  return axios.get('/questions', {params: params})
}

/**
 * 获取答案列表
 * @params {page, page_size, uid, pid}
 */
export const getAnswers = (params) => {
  return axios.get('/answers', {params: params})
}

/**
 * 提交问题答案
 * @params {pid, qid, uid, res}
 */
export const submitAnswers = (params) => {
  return axios.post('/answers', params)
}

/**
 * 编辑问题答案
 * @params {res}
 */
export const editorAnswers = (params) => {
  return axios.patch(`/answers/${params.pid}`, params)
}

/**
 * 删除答案
 * @params {pid}
 */
export const deleteAnswers = (params) => {
  return axios.delete(`/answers/${params.pid}`)
}

/**
 * 管理员新增用户
 * @params {name, email, password}
 */
export const addUsers = (params) => {
  return axios.post('/users', params)
}

/**
 * 管理员获取用户列表
 * @params {page, page_size}
 */
export const getUserList = (params) => {
  return axios.get('/users', {params: params})
}

/**
 * 管理员获取用户列表
 * @params {name, email, password}
 */
export const editorUser = (params) => {
  return axios.patch(`/users/${params.uid}`, params)
}

/**
 * 管理员获取用户列表
 * @params {uid}
 */
export const deleteUser = (params) => {
  return axios.delete(`/users/${params.uid}`)
}

/**
 * 创建项目
 * @params {name}
 */
export const createProject = (params) => {
  return axios.post('/projects', params)
}

/**
 * 获取项目列表
 * @params {page, page_size}
 */
export const getProjects = (params) => {
  return axios.get('/projects', {params: params})
}

/**
 * 编辑项目
 * @params {pid, name, }
 */
export const editorProject = (roots, params) => {
  return axios.patch(`/projects/${roots.pid}`, params)
}

/**
 * 删除项目
 * @params {pid}
 */
export const removeProject = (params) => {
  return axios.delete(`/projections/${params.pid}`)
}

/**
 * 获取当前项目的生成文档
 * @params {pid}
 */
export const getReports = (params) => {
  return axios.get(`/reports/${params.pid}`)
}

/**
 * 获取是否注册功能状态
 * @params {pid}
 */
export const getConfig = (params) => {
  return axios.get('/configs')
}

/**
 * 修改是否注册功能
 * @params {value}
 */
export const editorConfig = (params) => {
  return axios.patch(`/configs/${params.cid}`, params)
}