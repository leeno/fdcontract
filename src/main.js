// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import * as mUtils from 'utils/mUtils'
import Filters from 'utils/filter'
import VueCookies from 'vue-cookies'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

Vue.use(ElementUI)
Vue.use(Filters)
Vue.use(VueCookies)

Vue.config.productionTip = false

// 初始化sentry
Sentry.init({
  dsn: config.dsn,
  integrations: [
    new Integrations.Vue({
      Vue,
      attachProps: true
    })
  ]
})

/**
 * 如果用户刷新页面,导致存入vuex中的菜单数据清空,需要从缓存获取;
 */
const menuData = JSON.parse(localStorage.getItem('menuData'))
if (menuData) {
  // commit or dispatch ,将缓存数据注入到store中
  store.commit('ADD_MENU', menuData)
  // 根据菜单生成的路由信息
  const routes = mUtils.generateRoutesFromMenu(menuData)
  const asyncRouterMap = [
    {
      path: '/index',
      name: '首页',
      hidden: true,
      component: require('layout/Home.vue'),
      redirect: '/index',
      children: routes
    }
  ]
  router.addRoutes(asyncRouterMap)
}
router.beforeEach((route, redirect, next) => {
  if (route.path === '/projects' && route.query && route.query.uid && route.query.sid) {
    $cookies.set('sid', route.query.sid)
    $cookies.set('uid', route.query.uid)
  }
  // 定位到首页时，清空缓存数据;
  if (route.path === '/') {
    localStorage.removeItem('userinfo')
    localStorage.removeItem('menuData')
    store.commit('ADD_MENU', [])
  }

  // 判断是否为登陆后的回调界面并且有返回的cookie值
  if (route.path === '/callback' && mUtils.getCookie('uid') && mUtils.getCookie('uid') !== null) {
    next()
  } else {
    // 判断是否有用户登录的记录
    let userinfo = JSON.parse(localStorage.getItem('userinfo'))
    if (!userinfo && route.path !== '/') {
      // 没有用户信息，route.path不是定位到登录页面的,直接跳登录页面
      next({ path: '/' })
    } else {
      if (route.name) {
        // 有用户信息和路由名称的，直接跳要路由的页面
        next()
      } else {
        // 有用户信息，没有路由名称的，直接跳404页面
        next({ path: '/error' })
      }
    }
  }
})

/* eslint-disable no-new */
const globalRouter = new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

export default globalRouter
